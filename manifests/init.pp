class wordpress (
  $package_ensure = $wordpress::params::package_ensure,
  $path           = $wordpress::params::path,
  $htdocs         = $wordpress::params::htdocs,
  $wp_source      = $wordpress::params::wp_source,
) inherits wordpress::params {
  package { "subversion":
    ensure  => $package_ensure,
  }

  exec { "svn co $wp_source $htdocs":
    path => $path,
    require => Package["subversion"],
  }
}
