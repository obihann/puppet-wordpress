class wordpress::params {
  $package_ensure = "installed"
  $path           = "/usr/bin/"
  $htdocs         = "/var/www/html/"
  $wp_source      = "https://core.svn.wordpress.org/trunk/"
}

